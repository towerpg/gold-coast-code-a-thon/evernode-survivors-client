extends CanvasLayer


func _ready():
	$%PlayButton.pressed.connect(on_play_pressed)


func on_play_pressed():
	SilentWolf.Players.player_name = $%PlayerNameInput.text
	get_tree().change_scene_to_file("res://scenes/main/main.tscn")
