extends Area2D
class_name Hurtbox

@export var health: Node


func _ready():
	area_entered.connect(on_area_entered)


func on_area_entered(other_area: Area2D):
	if not other_area is Hitbox:
		return
	
	if health == null:
		return
	
	var hitbox = other_area as Hitbox
	health.damage(hitbox.damage)
