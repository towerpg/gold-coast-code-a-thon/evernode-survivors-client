extends Node


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func submitScore(playerName, score):
	
	print("I'm sending a new score to the Hot Pocket backend")
	var message = {} 
	message.userId = playerName
	message.score = score
	print(message)
	
	var http = HTTPRequest.new()
	get_tree().root.add_child(http)
	var body = JSON.stringify(message)
	var headers = ["Content-Type: application/json"]
	
	http.request("https://evernode-survivors-backend.onrender.com/evernode", headers, HTTPClient.METHOD_POST, body)
	#http.request("http://localhost:3001/evernode", headers, HTTPClient.METHOD_POST, body)
	var response = await http.request_completed
	print(response)
	response = response[3].get_string_from_utf8()
	return response
