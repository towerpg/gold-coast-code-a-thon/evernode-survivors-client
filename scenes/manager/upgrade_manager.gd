extends Node

@export var upgrade_pool: Array[AbilityUpgrade]
@export var experience_manager: Node
@export var upgrade_screen_scene: PackedScene

# This is a library to house all of the player's current upgrades.
var current_upgrades = {
}


func _ready():
	experience_manager.level_up.connect(on_level_up)

# This function runs when the Upgrade Manager recieves the level_up signal from the Experience Manager.
func on_level_up(current_level: int):
	var chosen_upgrade = upgrade_pool.pick_random()
	if chosen_upgrade == null:
		return
	
	# Instantiate the upgrade screen, and connect to the upgrade_screen script.
	var upgrade_screen_instance = upgrade_screen_scene.instantiate()
	add_child(upgrade_screen_instance)
	upgrade_screen_instance.set_ability_upgrades([chosen_upgrade] as Array[AbilityUpgrade])
	upgrade_screen_instance.upgrade_selected.connect(on_upgrade_selected)


# Add the selected upgrade to the player's current_upgrades library.
func apply_upgrade(upgrade: AbilityUpgrade):
	var has_upgrade = current_upgrades.has(upgrade.id)
	if not has_upgrade:
		current_upgrades[upgrade.id] = {
			"resource": upgrade,
			"quantity": 1
		}
	else:
		current_upgrades[upgrade.id]["quantity"] += 1
	
	GameEvents.emit_ability_upgrade_added(upgrade, current_upgrades)
	print(current_upgrades)


func on_upgrade_selected(upgrade: AbilityUpgrade):
	apply_upgrade(upgrade)
