extends CharacterBody2D


const MAX_SPEED = 100.0
const ACCELERATION_SMOOTHING = 5
var number_colliding_bodies = 0

@onready var damage_interval_timer = $DamageIntervalTimer
@onready var health = $Health
@onready var health_bar = $HealthBar

func _ready():
	$CollisionArea2D.body_entered.connect(on_body_entered)
	$CollisionArea2D.body_exited.connect(on_body_exited)
	damage_interval_timer.timeout.connect(on_damage_interval_timer_timeout)
	health.health_changed.connect(on_health_changed)
	update_health_display()


func _process(delta):
	var movement_vector = get_movement_vector()
	var direction = movement_vector.normalized()
	var target_velocity = direction * MAX_SPEED
	
	velocity = velocity.lerp(target_velocity, 1 - exp(-delta * ACCELERATION_SMOOTHING))
	if velocity.x != 0:
		$PlayerSprite.flip_h = velocity.x < 0
	move_and_slide()
	
	
func get_movement_vector():
	
	var x_movement = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	var y_movement = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	return Vector2(x_movement, y_movement)


func check_deal_damage():
	if number_colliding_bodies == 0 || !damage_interval_timer.is_stopped():
		return
	health.damage(1)
	damage_interval_timer.start()
	print(health.current_health)


func update_health_display():
	health_bar.value = health.get_health_percent()


func on_body_entered(other_body: Node2D):
	number_colliding_bodies += 1
	check_deal_damage()


func on_body_exited(other_body: Node2D):
	number_colliding_bodies -= 1


func on_damage_interval_timer_timeout():
	check_deal_damage()


func on_health_changed():
	update_health_display()

## Fun with Matrices and Transforms: https://docs.godotengine.org/en/stable/tutorials/math/matrices_and_transforms.html
## ToDo: Need to look into this documentation to make all Player child nodes respond
## to transforms of the parent. Things like feedback from taking damage, hitting stuff, etc. 
