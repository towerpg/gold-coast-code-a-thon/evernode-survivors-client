extends CanvasLayer

@export var score_manager: Node
@onready var label = $%Label

var score: int

func _ready():
	label.text = str(score)
	GameEvents.score_updated.connect(on_score_updated)


func on_score_updated(number):
	var score = number
	label.text = str(score)
