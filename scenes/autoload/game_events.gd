extends Node

signal experience_collected(number: float)
signal ability_upgrade_added(upgrade: AbilityUpgrade, current_upgrades: Dictionary)
signal score_collected(number: float)
signal score_updated(number: float)
signal final_score_calculated(number: float)


func emit_experience_collected(number: float):
	experience_collected.emit(number)


func emit_ability_upgrade_added(upgrade: AbilityUpgrade, current_upgrades: Dictionary):
	ability_upgrade_added.emit(upgrade, current_upgrades)


func emit_score_collected(number: float):
	score_collected.emit(number)


func emit_score_updated(number: float):
	score_updated.emit(number)


func emit_final_score_calculated(number: float):
	final_score_calculated.emit(number)
