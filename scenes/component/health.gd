extends Node
class_name Health

signal died
signal health_changed
signal score_changed(score: int)

@export var max_health = 10
@export var score_value = 100
var current_health
var score = 0


func _ready():
	current_health = max_health

func damage(damage: float):
	current_health = max(current_health - damage, 0)
	health_changed.emit()
	Callable(check_death).call_deferred()

func get_health_percent():
	if max_health <= 0:
		return
	return min (current_health / max_health, 1)


func check_death():
	if current_health == 0:
		died.emit()
		owner.queue_free()
