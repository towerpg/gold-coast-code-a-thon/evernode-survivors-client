extends AnimationPlayer


# Called when the node enters the scene tree for the first time.
func _ready():
	play("player_idle")
	
func _process(delta):
	if Input.is_action_pressed("move_up"):
		play("run")
	elif  Input.is_action_pressed("move_down"):
		play("run")
	elif  Input.is_action_pressed("move_left"):
		play("run")
	elif  Input.is_action_pressed("move_right"):
		play("run")
	else:
		play("player_idle")

