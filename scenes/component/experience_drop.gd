extends Node

@export_range(0, 1) var drop_percent: float = .75
@export var experience_scene: PackedScene
@export var health: Node


func _ready():
	(health as Health).died.connect(on_died)


func on_died():
	
	if randf() > drop_percent:
		return
	
	if experience_scene == null:
		return
	
	if not owner is Node2D:
		return
	
	var spawn_position = (owner as Node2D).global_position
	var experience_instance = experience_scene.instantiate() as Node2D
	var entities_layer = get_tree().get_first_node_in_group("entities_layer")
	entities_layer.add_child(experience_instance)
	experience_instance.global_position = spawn_position
