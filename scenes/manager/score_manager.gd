extends Node

signal score_updated(current_score: int)

var current_score = 0

func _ready():
	GameEvents.score_collected.connect(on_score_collected)


func increment_score(number: int):
	current_score = current_score + number
	GameEvents.emit_score_updated(current_score)


func on_score_collected(number: int):
	increment_score(number)
