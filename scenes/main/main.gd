extends Node


@export var end_screen_scene: PackedScene
var player_name
var score = 0

var websocket_url = "wss://evernode-survivors-backend.onrender.com"
var messageToSend = ""

@onready var _client : WebSocketClient = $"/root/LeaderboardWebsocket"

func _ready():
	$%Player.health.died.connect(on_player_died)
	print("Connecting to the websocket")
	_connect_to_websocket()
	SilentWolf.configure({
	"api_key": "tvYSTRHNQZ904ZfNvC3GTal7AKdX4NX55MaIrJCn",
	"game_id": "EvernodeSurvivors",
	"log_level": 0
  })
	#TODO: Need to get the main scene to open back up on close button click.
	SilentWolf.configure_scores({
	"open_scene_on_close": "res://scenes/main/main.tscn"
  })
	player_name = SilentWolf.Players.player_name
	GameEvents.score_collected.connect(increment_score)
 
func _connect_to_websocket():
	print(websocket_url)
	var error = _client.connect_to_url(websocket_url)
	if error != OK:
		print("Error connecting to websocket: %s" % [websocket_url])

func increment_score(number):
	score = score + number


func on_player_died():
	LeaderboardCallouts.submitScore(player_name, score)
	SilentWolf.Scores.save_score(player_name, score)
	var end_screen_instance = end_screen_scene.instantiate()
	add_child(end_screen_instance)
