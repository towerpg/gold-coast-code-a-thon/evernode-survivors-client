extends Node

@export var health: Node
@export var score_value = 100


func _ready():
	(health as Health).died.connect(on_died)

func on_died():
	GameEvents.emit_score_collected(score_value)
