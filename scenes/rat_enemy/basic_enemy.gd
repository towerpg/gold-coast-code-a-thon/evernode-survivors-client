extends CharacterBody2D

const MAX_SPEED = 45

@onready var health: Health = $Health


func _process(delta):
	var direction = get_direction_to_player()
	velocity = direction * MAX_SPEED
	if velocity.x != 0:
		$Sprite2D.flip_h = velocity.x < 0
	move_and_slide()

func get_direction_to_player():
	var player_node = get_tree().get_first_node_in_group("player") as Node2D
	if player_node != null:
		return (player_node.position - global_position).normalized()
	return Vector2.ZERO
